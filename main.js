(function () {
    var persons = {
        '886eeba1-196a-430e-8b37-9e8cb3245ec': {
            name: 'Б-------',
            lastName: 'В--------',
            secondName: 'Г---------',
            birthDate: '25.01.1995',
            ps1: '01--',
            ps2: '---408',
        }
    }
    function GET(key) {
        var p = window.location.search;
        p = p.match(new RegExp(key + '=([^&=]+)'));
        return p ? decodeURI(p[1]) : false;
    }
    
    function format(str) {
        var newStr = str[0];
        for (var i = 0; i < str.length - 1; i++) {
            newStr = newStr + '*';
        }

        return newStr
    }

    function formatForPs(str, start, length) {
        var newStr = '';
        for (var i = 0; i < str.length; i++) {
            if (i >= start && i < start + length) {
                newStr = newStr + str[i];
            } else {
                newStr = newStr + '*';
            }
        }

        return newStr
    }

    const person = persons[GET('id')];

    var name = format(person.name);
    var lastName = format(person.lastName);
    var secondName = format(person.secondName);
    var birthDate = person.birthDate;
    var ps1 = formatForPs(person.ps1, 0, 2);
    var ps2 = formatForPs(person.ps2, 3, 3);

    document.getElementById('name').innerText = lastName + ' ' + name + ' ' + secondName;
    document.getElementById('birthDate').innerText = birthDate;
    document.getElementById('passport').innerText = ps1 + ' ' + ps2;
})();
