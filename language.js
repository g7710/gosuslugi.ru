var currentLanguage = 'ru';

var languagePreset = {
    en: {
        'lang-name': 'ENG',
        'text-certificate': 'COVID-19 certificate',
        'text-valid': 'Valid',
        'text-valid-until': 'Valid until: ',
        'text-name': 'Name',
        'text-birth': 'Date of birth: ',
        'text-passport': 'National passport: ',
        'text-close': 'Close',
    },
    ru: {
        'lang-name': 'RUS',
        'text-certificate': 'Сертификат COVID-19',
        'text-valid': 'Действителен',
        'text-valid-until': 'Действует до: ',
        'text-name': 'ФИО',
        'text-birth': 'Дата рождения: ',
        'text-passport': 'Паспорт: ',
        'text-close': 'Закрыть',
    },
}

function switchLanguage() {
    currentLanguage = currentLanguage === 'ru' ? 'en' : 'ru';

    document.getElementById('lang-img').classList.remove('en');
    document.getElementById('lang-img').classList.remove('ru');

    document.getElementById('lang-img').classList.add(currentLanguage);

    const preset = languagePreset[currentLanguage];

    Object.keys(preset).forEach(function (key) {
        document.getElementById(key).innerText = preset[key];
    })
}
